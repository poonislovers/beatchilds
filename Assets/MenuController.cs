﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuController : MonoBehaviour
{
    public SpawnFallingObject fallingObject;
    public GameObject boss;
    public GameManager gm;
    public GameObject canvas;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame(float difficulty)
    {
        //turn boss on after everything
        fallingObject.fall = true;
        canvas.SetActive(false);
        gm.MovementSurge(0.5f);
        gm.difficultyMultiplier = difficulty;
        gm.gameHasStarted = true;
        boss.SetActive(true);
        //add start game script
    }
}
