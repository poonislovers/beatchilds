﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    public BigBadBossScript bbbs;
    public SimpleHealthBar healthBar;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.UpdateBar(bbbs.curHealth, bbbs.maxHealth);
    }
}
