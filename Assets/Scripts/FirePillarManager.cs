﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePillarManager : MonoBehaviour
{
    public FirePillar[] firePillar = new FirePillar[8];
    public enum PatternType {Random,Pattern1}
    public PatternType currentType;
    private int counter = 0,counter2 = 0;
    public bool attack = false,reset = false;
    public bool[] played = new bool[8];
    public bool randomPlayed = false;

    //how long random attack last for at start
    public float randomPlayingDuration = 15;
    //how long random attack last for after start
    public float setPlayingDuration;
    // Start is called before the first frame update
    void Start()
    {
        setPlayingDuration = randomPlayingDuration;
    }

    // Update is called once per frame
    void Update()
    {
        switch (currentType)
        {
            case PatternType.Random://random attack
                if (attack && randomPlayingDuration >= 0)
                {
                    if (!randomPlayed)//if not played
                    {
                        //get random number
                        counter = Random.Range(0, firePillar.Length);
                        //play that pillar
                        firePillar[counter].play = true;
                        counter2 = Random.Range(0, firePillar.Length);
                        //play another pillar that is not first pillar
                        while (counter2 == counter)
                        {
                            counter2 = Random.Range(0, firePillar.Length);
                        }
                        firePillar[counter2].play = true;
                        randomPlayed = true;
                    }
                    else
                    {
                        if (firePillar[counter].finishPlaying)//when pillar finish playing played = false
                        {
                            randomPlayed = false;
                        }
                    }

                }
                if(attack)//if attacking duration decrease
                randomPlayingDuration -= Time.deltaTime;
                    break;
            case PatternType.Pattern1:
                if (attack)
                {
                    for(int i = 0;i < firePillar.Length; i++)
                    {
                        if(i == 0)//if first pillar
                        {
                            if (!played[i])
                            {
                                firePillar[i].play = true;
                                played[i] = true;
                            }

                        }else if (firePillar[i - 1].finishPlaying && played[i-1])//check previous pillar if its finished then play
                        {
                            firePillar[i - 1].finishPlaying = false;
                            if (!played[i])
                            {
                                firePillar[i].play = true;
                                played[i] = true;
                            }
                        }
                    }
                }
                break;
        }

        if (reset)
        {
            resetFirePillar();
            reset = !reset;
        }

    }

    public void resetFirePillar()
    {
        //reset all booleanm and duration and plays.
        randomPlayingDuration = setPlayingDuration;
        randomPlayed = false;
        attack = false;
        for(int i = 0; i < played.Length; i++)
        {
            firePillar[i].finishPlaying = false;
            played[i] = false;
        }
    }

    public void StartFirePillar()
    {
        reset = true;
        StartCoroutine(FirePillarAttack());
    }

    IEnumerator FirePillarAttack()
    {
        yield return new WaitForSeconds(0.1f);
        attack = true;
    }

    

}
