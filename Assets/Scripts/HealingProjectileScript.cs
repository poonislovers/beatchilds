﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingProjectileScript : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 10;
    public float healAmount;
    private Vector3 targetedPosition;
    private GameObject boss;

    // Start is called before the first frame update
    void Start()
    {
        boss = GameObject.FindGameObjectWithTag("BigBadBoss");
        targetedPosition = boss.transform.position;
        this.transform.LookAt(boss.transform);
        Destroy(this.gameObject, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * moveSpeed * GameManager.Instance.globalSpeedVariable;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("BigBadBoss"))
        {
            BigBadBossScript bbbs = collision.gameObject.GetComponent<BigBadBossScript>();
            bbbs.Heal(healAmount);
            Destroy(this.gameObject);
        }
    }
}
