﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBirdScript : MonoBehaviour
{
    private GameObject player;
    private GameManager gm;
    [SerializeField]
    private AudioSource m_as;
    [SerializeField]
    private AudioClip shootingSound;
    public Animator m_Animator;
    private float timer;
    [SerializeField]
    private float curHealth;
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private GameObject firingPoint;
    [SerializeField]
    private float shootingSpeed;
    [SerializeField]
    private float damage;
    [SerializeField]
    private bool isDead;
    [SerializeField]
    private bool lookAtPlayer;


    private void Awake()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        //diffculty affects the health of the enemy
        curHealth *= gm.difficultyMultiplier;
        //FUCK YOU GAMe
        damage *= gm.difficultyMultiplier;

    }

    // Update is called once per frame
    void Update()
    {
        //look at the god damn player
        if (lookAtPlayer && !isDead)
        {
            //look at the player
            SmoothLook(player.transform.position);
            if (timer >= shootingSpeed * gm.currentGameState)
            {

                m_Animator.SetTrigger("Shoot");
                timer = 0;
            }

        }
        //this shit be based of the game speed;
        m_Animator.speed = gm.globalSpeedVariable / gm.currentGameState;
        timer += Time.deltaTime * gm.globalSpeedVariable;
    }

    private void SmoothLook(Vector3 location)
    {
        //look nicely towards player
        var targetRot = Quaternion.LookRotation(location - this.transform.position);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, targetRot, Time.deltaTime * gm.globalSpeedVariable/gm.currentGameState);
    }

    public void Shoot()
    {
        //PEW PEW PEW
        m_as.PlayOneShot(shootingSound);
        GameObject boolet = Instantiate(bullet, firingPoint.transform.position, Quaternion.identity);
        boolet.GetComponent<ProjectileFlyingScript>().damage = this.damage;
    }

    public void TakeDamage(float damageTaken)
    {
        this.curHealth -= damageTaken;
        if (this.curHealth <= 0)
        {
            //death is real
            if (!isDead)
            {
                m_Animator.SetTrigger("Dead");
                isDead = true;
            }
        }
    }


}
