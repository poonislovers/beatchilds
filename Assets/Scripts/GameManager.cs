﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using VRTK;
public class GameManager : MonoBehaviour
{

    public static GameManager Instance;
    private BigBadBossScript boss;
    [SerializeField]
    private AudioSource m_as;
    [SerializeField]
    private AudioClip insaneBGM;
    [SerializeField]
    private AudioClip loseJingle,winJingle;
    [SerializeField]
    private FirePillarManager firePillarManager; //this shit fucking sucks bcuz someone called jarran coded it suck my blue balls
    [SerializeField]
    private FallingObjectManager anchorManager; // fuck you jarran
    //everything that moves and flies besides the player should * this variable
    [Range(0f,1f)]
    public float globalSpeedVariable;
    [Range(1, 2)]
    public float difficultyMultiplier = 1;
    [Range(0.1f, 1f)]
    public float currentGameState = 1f;
    [SerializeField]
    private bool healingEnabledForBoss = false;
    [SerializeField]
    private GameObject obeliskPrefab;
    [SerializeField]
    private bool allowedToSpawnObelisk = true;
    public int obeliskSpawnTime = 10;
    [SerializeField]
    private List<GameObject> obeliskSpawnPoints;
    [SerializeField]
    private List<GameObject> obeliskAlive;
    [SerializeField]
    private bool gameEnded = false;
    [SerializeField]
    private float timer;
    private float anchorTimer;
    public bool movementSurge;
    public bool gameHasStarted;

    private bool dejavu;
    [SerializeField]
    private bool death;
    [SerializeField]
    private bool win;

    public VRTK_HeadsetFade fade;
    private void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(!gameEnded){ 
        if (boss == null && gameHasStarted)
        {
            boss = GameObject.FindGameObjectWithTag("BigBadBoss").GetComponent<BigBadBossScript>();
        }
        if (movementSurge)
        {
            //for testing
            movementSurge = false;
            MovementSurge();
        }
        anchorTimer += Time.deltaTime * globalSpeedVariable / currentGameState;
        //adding time for spawning
        if (healingEnabledForBoss && allowedToSpawnObelisk)
        {
            timer += Time.deltaTime * globalSpeedVariable / currentGameState;
            if (timer >= obeliskSpawnTime * currentGameState)
            {
                Debug.Log("fuck it");
                timer = 0;
                //summon the obelisk out
                for (int i = 0; i < obeliskSpawnPoints.Count; i++)
                {
                    //summon the obelisk out for fun duh
                    GameObject obelisk = Instantiate(obeliskPrefab, obeliskSpawnPoints[i].transform.position, Quaternion.identity);
                    obeliskAlive.Add(obelisk);
                }
                allowedToSpawnObelisk = false;
            }
        }

        if (healingEnabledForBoss)
        {
            //check the obelisk and see if theres 0 left, once there is none left allow spawning again
            obeliskAlive.RemoveAll(item => item == null);
            if (obeliskAlive.Count == 0)
            {
                allowedToSpawnObelisk = true;
            }
        }

        //state machine to change game difficulty
        //based on missing % of hp
        if (boss != null)
            currentGameState = (boss.curHealth / boss.maxHealth);
        if (currentGameState <= 0.25f)
        {
            currentGameState = 0.25f;
            if (!dejavu)
            {
                dejavu = true;
                Dejavu();
            }
        }

        if (anchorTimer >= 1)
        {
            //spawn shit
            anchorManager.spawn = true;
            anchorTimer = 0;
        }
    }
        if (death)
        {
            globalSpeedVariable = 0;
            m_as.volume -= 0.001f;
            if(m_as.volume == 0)
            {
                death = false;
                m_as.Stop();
                m_as.volume = 0.25f;
                m_as.PlayOneShot(loseJingle);
                StartCoroutine(AvengerEndGame());
            }
        }
        if (win)
        {
            globalSpeedVariable = 0;
            m_as.volume -= 0.001f;
            fade.Fade(Color.black, 2f);
            if (m_as.volume == 0)
            {
                win = false;
                m_as.Stop();
                m_as.volume = 0.25f;
                m_as.PlayOneShot(winJingle);
                StartCoroutine(AvengerEndGame());
            }
        }

    }

    public void MovementSurge()
    {
        //make game fast and then slow
        StartCoroutine(OneTickMovement());
        
    }

    public void MovementSurge(float timing)
    {
        //make game fast and then slow
        StartCoroutine(OneTickMovement(timing));

    }

    public void FastGame()
    {
        //fast time
        globalSpeedVariable = 1f;
    }

    public void SlowGame()
    {
        //slow time
        globalSpeedVariable = 0.2f;
    }

    IEnumerator OneTickMovement()
    {
        //a surge
        FastGame();
        yield return new WaitForSeconds(0.2f);
        SlowGame();
    }

    IEnumerator OneTickMovement(float timing)
    {
        //a surge
        FastGame();
        yield return new WaitForSeconds(timing);
        SlowGame();
    }

    public void SpecialBossAttack(int x)
    {
        //choose an attack
        if(x == 0)
        {
            firePillarManager.currentType = FirePillarManager.PatternType.Pattern1;
        }
        else
        {
            firePillarManager.currentType = FirePillarManager.PatternType.Random;
        }
        Debug.Log("penis");
        //start the attack
        firePillarManager.StartFirePillar();
        //i blame jarran for this mess

    }

    public void EnableHealing()
    {
        //just allow healing now
        healingEnabledForBoss = true;
    }

    public void Dejavu()
    {
        //DEJAVU
        m_as.Stop();
        m_as.clip = insaneBGM;
        m_as.Play();
    }

    public void GameWin()
    {
        Debug.Log("fuck you");
        gameEnded = true;
        //win game effects?
        win = true;

    }

    public void GameLose()
    {
        gameEnded = true;
        //gg u lost
        death = true;
    }

    public IEnumerator AvengerEndGame()
    {
        yield return new WaitForSeconds(3);
        Destroy(GameObject.FindGameObjectWithTag("VRTK"));
        Application.LoadLevel(Application.loadedLevel);
    }


}
