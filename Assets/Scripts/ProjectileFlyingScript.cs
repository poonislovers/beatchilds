﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileFlyingScript : MonoBehaviour
{
    private GameObject player;
    private Vector3 targetedPosition;
    private GameManager gm;
    [SerializeField]
    private float moveSpeed = 1;
    public float damage;

    private void Awake()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        this.transform.LookAt(player.transform);
        Destroy(this.gameObject, 10f);
    }

    // Start is called before the first frame update
    void Start()
    {
        targetedPosition = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //move towards target;
        transform.position += transform.forward * Time.deltaTime * moveSpeed * gm.globalSpeedVariable;
    }

    void OnCollisionEnter(Collision collision)
    {
        //detect damn collison
        if (collision.gameObject.CompareTag("Player"))
        {
            Player player = collision.gameObject.GetComponent<Player>();
            player.TakeDamage(damage);
            Destroy(this.gameObject);
        }
        else if(collision.gameObject.CompareTag("Platform"))
        {
            //play a sound here?
            Destroy(this.gameObject);
        }
    }
}
