﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MusicIntrotoLoop : MonoBehaviour {
    public AudioSource Intro, Loop;
    public bool played = false;
    private bool reached = false;
    public float limit = 1;
    // Use this for initialization
    //our songs comes in 2 parts, intro and loop this is to make it play intro into loop
    void Start () {
        Intro.volume = 0;
        Loop.volume = limit;

    }
	
	// Update is called once per frame
	void Update () {
        if(Intro.volume<limit)
        Intro.volume += 0.001f;
		if (!Intro.isPlaying && !played)
        {
            Loop.Play();
            played = true;
        }    
        if(Loop.volume <= 0)
        {
            reached = true;
            Loop.Stop();
            if( (!Intro.isPlaying))
            {
                Intro.volume = 0;
                Intro.Play();
            }
            if (Intro.volume < limit)
                Intro.volume += 0.001f;
        }        
    }
}
