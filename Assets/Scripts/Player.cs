﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Player : MonoBehaviour
{
    private GameManager gm;
    [SerializeField]
    public float curHealth;
    public float maxHealth;
    [SerializeField]
    private float damage;
    [SerializeField]
    private GunScript leftGS;
    [SerializeField]
    private GunScript rightGS;
    private int maxBulletCount;
    [SerializeField]
    private int leftBulletCount;
    [SerializeField]
    private int rightBulletCount;
    [SerializeField]
    private float leftReloadCounter;
    [SerializeField]
    private float rightReloadCounter;
    private float maxReloadCounter;
    [SerializeField]
    private GameObject leftHand;
    [SerializeField]
    private GameObject rightHand;
    public bool isDead;
    [SerializeField]
    public float exp;
    [SerializeField]
    public float requiredExpToLevel;
    private float timer;
    [SerializeField]
    private VRTK_HeadsetFade fade;

    [SerializeField]
    private AudioSource m_as;
    [SerializeField]
    private AudioClip shootingSound;
    [SerializeField]
    private AudioClip reloadingSound;
    [SerializeField]
    private AudioClip levelingSound;
    [SerializeField]
    private AudioClip damageTakenSound;

    //player not affected by game speed but by gamemanager's difficulty multipier

    private void Awake()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        maxHealth = curHealth; //set original max health
        maxBulletCount = leftBulletCount;
        maxReloadCounter = leftReloadCounter;
        leftHand = VRTK_DeviceFinder.GetControllerLeftHand(true);
        rightHand = VRTK_DeviceFinder.GetControllerRightHand(true);
    }

    public void TakeDamage(float damageTaken)
    {
        this.curHealth -= damageTaken;
        m_as.PlayOneShot(damageTakenSound);
        if(this.curHealth <= 0)
        {
            //die // but u gotta add that somewhere // maybe pause the game and then just headset fade //good idea dwayne
            
            gm.globalSpeedVariable = 0.01f;
            fade.Fade(Color.red, 2f);
            gm.GameLose();
            isDead = true;
            //if u dead u cant shoot so i cant bug the fucking game
        }
    }

    public void Heal()
    {
        //bitch i think we just heal whenever u level up
        curHealth = maxHealth;
    }

    public void LevelUp()
    {
        //set exp to 0, health to max, and required exp to x2 * gamemulitpier, damage increase too
        exp = 0;
        m_as.PlayOneShot(levelingSound);
        requiredExpToLevel *= 2 * gm.difficultyMultiplier;
        damage *= 1.25f;
        //increase and then cast
        damage = (int)damage;
        Heal();
    }

    public void GainEXP(float expGained)
    {
        //gaining exp muh
        exp += expGained;
    }

    public void LeftShoot()
    {
        if (leftBulletCount <= 0)
        {
            //items empy u bbaboon
            //play an empty sound?
            //needs a damn reload
            Debug.Log("reload ur damn left gun");
        }
        else
        {
            m_as.PlayOneShot(shootingSound);
            leftBulletCount--;
            //call when trigger button is shot on the left, should be the same as right shootcopy paste
            leftGS.Shoot(this.damage);
            //pew pew
        }
    }

    public void RightShoot()
    {
        gm.MovementSurge();
        if (rightBulletCount <= 0)
        {
            //items empy u bbaboon
            //play an empty sound?
            //needs a damn reload
            Debug.Log("reload ur damn Right gun");
        }
        else
        {
            m_as.PlayOneShot(shootingSound);
            rightBulletCount--;
            //call when trigger button is shot on the left, should be the same as right shootcopy paste
            rightGS.Shoot(this.damage);
            //pew pew
        }
    }

    public void LeftReload()
    {
        //call when rotated to reload
        m_as.PlayOneShot(reloadingSound,0.25f);
        leftBulletCount = maxBulletCount;
        gm.MovementSurge(0.5f);
        //pew pew
    }
    public void RightReload()
    {
        //call when rotated to reload
        m_as.PlayOneShot(reloadingSound,0.25f);
        rightBulletCount = maxBulletCount;
        gm.MovementSurge(0.5f);
        //pew pew
    }

    private void Update()
    {
        timer += Time.deltaTime * gm.globalSpeedVariable;

        if (timer >= 1)
        {
            leftReloadCounter--;
            rightReloadCounter--;
            timer = 0;
        }

        //for leveling up 
        if(exp >= requiredExpToLevel)
        {
            LevelUp();
        }

        //do a 90 degree turn to reload ur left gun
        if((leftHand.transform.localEulerAngles.z >= 80 && leftHand.transform.localEulerAngles.z <= 100) && leftBulletCount != maxBulletCount && leftReloadCounter <= 0)
        {
            leftReloadCounter = maxReloadCounter;
            LeftReload();
        }
        if((rightHand.transform.localEulerAngles.z >= 80 && rightHand.transform.localEulerAngles.z <= 100) && rightBulletCount != maxBulletCount && rightReloadCounter <= 0)
        {
            rightReloadCounter = maxReloadCounter;
            RightReload();
        }
        
    }
}
