﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletScript : MonoBehaviour
{
    private Player player;
    public float damage;
    public float moveSpeed = 10;
    public GameManager gm;

    void Awake()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        Destroy(this.gameObject, 10f);
    }

    void Update()
    {
        //move forward balkbhabal
        transform.position += transform.forward * Time.deltaTime * moveSpeed;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Harpy"))
        {
            //harpy
            FlyingBirdScript fbs = collision.gameObject.GetComponent<FlyingBirdScript>();
            player.GainEXP(damage);
            fbs.TakeDamage(damage);

        }
        else if (collision.gameObject.CompareTag("BigBadBoss"))
        {
            //boss
            BigBadBossScript bbbs = collision.gameObject.GetComponent<BigBadBossScript>();
            player.GainEXP(damage);
            bbbs.TakeDamage(damage);

        }
        else if (collision.gameObject.CompareTag("ETC"))
        {
            //obelisk or enemy bullet
            if (collision.gameObject.GetComponent<Destructable>().canBeDestroyed)
            {
                Destroy(collision.gameObject);
            }

        }
        else if (collision.gameObject.CompareTag("Obelisk"))
        {
            if (collision.gameObject.GetComponent<Destructable>().canBeDestroyed)
            {
                //need to destroy the main gameobject;
                player.GainEXP(damage);
                Destroy(collision.transform.parent.gameObject);

            }
        }
        Destroy(this.gameObject);
    }
}

