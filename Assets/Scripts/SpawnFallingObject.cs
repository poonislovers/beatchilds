﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class SpawnFallingObject : MonoBehaviour
{
    public GameObject[] fallingObject;
    public GameObject createdObject;
    public bool fall = false;
    public bool falling;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fall)
        {
            createdObject = Instantiate(fallingObject[UnityEngine.Random.Range(0,fallingObject.Length)], transform.position, transform.rotation);
            falling = true;
            createdObject.GetComponent<FallingAnchorScript>().sfo = this;
            fall = !fall;
        }
    }
}
