﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingAnchorScript : MonoBehaviour
{
    [SerializeField]
    private float damage;
    [SerializeField]
    private float moveSpeed;
    public SpawnFallingObject sfo;
    FallingObjectManager fom;

    private void Update()
    {
        this.transform.position += Vector3.down * Time.deltaTime * GameManager.Instance.globalSpeedVariable * moveSpeed;
    }

    private void Start()
    {
        fom = GameObject.FindGameObjectWithTag("PlatformManager").GetComponent<FallingObjectManager>();
        fom.counter++;
        damage = (int)(damage * GameManager.Instance.difficultyMultiplier);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            //hit the damn player
            collision.gameObject.GetComponent<Player>().TakeDamage(damage);

        }
        Debug.Log("penis");
        sfo.falling = false;
        fom.counter--;
        Destroy(this.gameObject);
    }
}
