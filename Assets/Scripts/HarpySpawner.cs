﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class HarpySpawner : MonoBehaviour
{
    public GameObject[] harpiesNearby;
    public GameObject[] harpies;
    public GameObject[] spawners;
    public GameObject harpy;

    public float spawnRate;
    private float setSpawnRate;
    public int maxSpawn;

    public int currentSpawner;

    // Update is called once per frame
    void Update()
    {
        spawnRate -= Time.deltaTime * GameManager.Instance.globalSpeedVariable;
        if (!findHarpiesAround() && spawnRate <= 0)//if not enough harpies around and spawn rate is 0
        {
            //choose random spawner and spawn
            currentSpawner = UnityEngine.Random.Range(0, spawners.Length);
            spawnRate = setSpawnRate * GameManager.Instance.currentGameState;
            Instantiate(harpy, spawners[currentSpawner].transform.position, spawners[currentSpawner].transform.rotation);
        }
       
    }

    void Start()
    {
        setSpawnRate = spawnRate;    
    }

    public bool findHarpiesAround()
    {
        harpiesNearby = new GameObject[maxSpawn];//get nearby harpies
        harpies = GameObject.FindGameObjectsWithTag("Harpy");
        int i = 0, count = 0;
        foreach(GameObject obj in harpies)//for each harpy check distance and if near enough set as an object in the array
        {
            try
            {
                float distance = (obj.transform.position - transform.position).magnitude;
                if(distance < 100)
                {
                    harpiesNearby[i] = obj;
                    i++;
                }
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }
        for(int j = 0;j < harpiesNearby.Length; j++)//count++ for each harpy
        {
            if(harpiesNearby[j] != null)
            {
                count++;
            }
        }
        if(count >= maxSpawn)//check if count is more than or equal to max else false.
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
