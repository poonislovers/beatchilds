﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePillar : MonoBehaviour
{
    public ParticleSystem firePillar;
    public GameObject warning;
    public bool play = false, finishPlaying = false;
    public float timeTakenToFinish = 3,warningTime = 3;
    public FirePillarManager FPM;
    public BoxCollider bc;
    public Player player;
    public int damage = 100;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        warning.SetActive(false);
        bc.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (play)//if play is true
        {
            damage = (int)(damage * GameManager.Instance.difficultyMultiplier);
            finishPlaying = false;
            //set warning and play particles
            warning.SetActive(true);
            StartCoroutine(playParticles());
            play = !play;
        }
      
    }
    //playing particle coroutine
    public IEnumerator playParticles()
    {
        yield return new WaitForSeconds(warningTime);
        warning.SetActive(false);
        firePillar.Play();
        bc.enabled = true;
        yield return new WaitForSeconds(timeTakenToFinish);
        bc.enabled = false;
        if (FPM.attack)
        {
            finishPlaying = true;
        }
    }
    //damage player
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("Damage");
            player.TakeDamage(damage);
        }
    }
}
