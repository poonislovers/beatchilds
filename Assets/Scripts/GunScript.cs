﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    
    [SerializeField]
    private GameObject firingPoint;
    [SerializeField]
    private GameObject bulletPrefab;

    public void Shoot(float damage)
    {
        //pew pew pew
        GameObject bullet = Instantiate(bulletPrefab, firingPoint.transform.position, firingPoint.transform.rotation);
        bullet.GetComponent<PlayerBulletScript>().damage = damage;
    }

}
