﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObeliskScript : MonoBehaviour
{
    public List<GameObject> crystals;
    public Destructable destructable;
    public float healingTimer;
    public float healAmount;
    public GameObject firingPoint;
    [SerializeField]
    private GameObject healingObject;
    [SerializeField]
    private float timer;
    [SerializeField]
    private GameObject boss;

    private void Awake()
    {
        destructable = GetComponent<Destructable>();
        boss = GameObject.FindGameObjectWithTag("BigBadBoss");
        healAmount = (int)(healAmount * GameManager.Instance.difficultyMultiplier);
    }


    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime * GameManager.Instance.globalSpeedVariable;
        //if theres no more crystals, can be destroyed;
        if(crystals.Count == 0)
        {
            //pew pew pewww can be blown up
            destructable.canBeDestroyed = true;
        }
        //check if crystals are empty
        crystals.RemoveAll(item => item == null);

        if(timer >= healingTimer * GameManager.Instance.currentGameState)
        {
            timer = 0;
            GameObject healingProjectile = Instantiate(healingObject, firingPoint.transform.position, Quaternion.identity);
            healingProjectile.GetComponent<HealingProjectileScript>().healAmount = this.healAmount;
        }
    }
}
