﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigBadBossScript : MonoBehaviour
{
    private GameObject player;
    private GameManager gm;
    public Animator m_Animator;
    private float timer;
    private float specialTimer;
    [SerializeField]
    public float curHealth;
    public float maxHealth;
    [SerializeField]
    private GameObject bullet;
    [SerializeField]
    private GameObject firingPoint;
    [SerializeField]
    public float shootingSpeed;
    public float specialAttackCooldown;
    private float specialAttackRealCD;
    [SerializeField]
    private float damage;
    [SerializeField]
    private bool isDead;
    [SerializeField]
    private bool doingSpecialAttack;
    [SerializeField]
    private bool lookAtPlayer;
    [SerializeField]
    private AudioSource m_as;
    [SerializeField]
    private AudioClip shootinSound;
    [SerializeField]
    private AudioClip specialAttackSound1;
    [SerializeField]
    private AudioClip specialAttackSound2;


    void Awake()
    {
        gm = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        //diffculty affects the health of the enemy
        curHealth *= gm.difficultyMultiplier;
        //set the max cds
        maxHealth = curHealth;
        specialAttackRealCD = specialAttackCooldown;
        //fcuk you game
        damage *= gm.difficultyMultiplier;
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //look at the god damn player
        if (lookAtPlayer && !isDead)
        {
            //look at the player
            SmoothLook(player.transform.position);
            //affected by curretgamestate to allow the shooting to be faster
            if (timer >= shootingSpeed * gm.currentGameState)
            {
                
                m_Animator.SetTrigger("Shoot");
                timer = 0;
            }
            if(specialAttackCooldown <= 0 && curHealth < (maxHealth/100 * 75))
            {
                //put it on cd
                doingSpecialAttack = true;
                specialAttackCooldown = specialAttackRealCD;
                m_Animator.SetInteger("State", 1);
                gm.EnableHealing(); //doesnt matter if it gets called like 100000 times, its finnnnnnne
                //if hp below 75% starts to have special attack like healing and shit
            }

        }
        //this shit be based of the game speed;
        m_Animator.speed = gm.globalSpeedVariable / gm.currentGameState;
        timer += Time.deltaTime * gm.globalSpeedVariable;
        //handling the special attack timer
        specialTimer += Time.deltaTime * (gm.globalSpeedVariable / gm.currentGameState);
        if(specialTimer >= 1  && !doingSpecialAttack)
        {
            specialTimer = 0;
            specialAttackCooldown--;
        }
    }

    private void SmoothLook(Vector3 location)
    {
        //look nicely towards player
        var targetRot = Quaternion.LookRotation(location - this.transform.position);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, targetRot, Time.deltaTime * gm.globalSpeedVariable / gm.currentGameState);
    }

    public void Shoot()
    {
        //PEW PEW PEW
        m_as.PlayOneShot(shootinSound);
        GameObject boolet =Instantiate(bullet, firingPoint.transform.position, Quaternion.identity);
        boolet.GetComponent<ProjectileFlyingScript>().damage = this.damage;
    }

    public void TakeDamage(float damageTaken)
    {
        this.curHealth -= damageTaken;
        if (this.curHealth <= 0)
        {
            //death is real boys death is real
            if (!isDead)
            {
                m_Animator.SetTrigger("Dead");
                gm.GameWin();
                isDead = true;
            }
        }
    }

    public void SpecialAttack()
    {
        //do a random.range to get a random patter
        int x = Random.Range(0, 2);
        if(x == 0)
        {
            //play a sound
            m_as.PlayOneShot(specialAttackSound1);
        }
        else
        {
            //play another sound
            m_as.PlayOneShot(specialAttackSound2);
        }
        gm.SpecialBossAttack(x);
    }

    public void Heal(float healAmount)
    {
        //just add ezpz
        curHealth += healAmount;
        if(curHealth >= maxHealth)
        {
            curHealth = maxHealth;
        }
    }

    public void EndOfSpecialAttack()
    {
        //bool to stop timer from minsuing is now disabled
        doingSpecialAttack = false;
        m_Animator.SetInteger("State", 0);
    }

    public void KillObject()
    {
        Destroy(this.gameObject);
    }

    
}
