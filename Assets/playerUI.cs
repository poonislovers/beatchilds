﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerUI : MonoBehaviour
{
    public SimpleHealthBar healthBar;
    public Player player;
    public bool isHealth;
    // Start is called before the first frame update
    void Start()
    {
        healthBar = this.GetComponent<SimpleHealthBar>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isHealth)
        {
            healthBar.UpdateBar(player.curHealth, player.maxHealth);
        }
        else
        {
            healthBar.UpdateBar(player.exp, player.requiredExpToLevel);
        }

    }
}
