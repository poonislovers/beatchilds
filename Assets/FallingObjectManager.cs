﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingObjectManager : MonoBehaviour
{
    public SpawnFallingObject[] sfo;
    public bool spawn;
    public int counter;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        while(spawn)
        {
            if(counter <= 7)
            {
                Spawn();
            }
            else
            {
                spawn = false;
            }

        }
     }

    public void Spawn()
    {
        int i = Random.Range(0, sfo.Length);
        if (!sfo[i].falling)
        {
            sfo[i].fall = true;
            spawn = false;
        }
    }
    }

